# hybrid_mess_exio_5reg

This repository includes the example used in the manuscript:

**Coupling Macro-Energy Models with Multi-Regional Input-Output Models based on the Make and Use Framework - insights from Messageix and Exiobase**
, submitted to Economic Systems Research (Special issue: Integrated Assessment Models and Input-Output Analysis)

The example combines a macro ESM with a MRIO model based on the Make and Use framework. 
macro ESM: Messageix ENGAGE_SSP2_v4.1.2_R12, year 2020
MRIO: Exiobase version 3.8, year 2020 [DOI 10.5281/zenodo.5589597](https://zenodo.org/record/5589597)
NOTE: When using this repository, note the licences of the original datasets.

To couple both models based on this framework, some preprocessing is necessary, including the transformation of Messageix results into make and use tables and the aggregation of both models into five-region versions.
Furthermore, the scripts of this repository include the hybridization of both models as well as the cration of figures for the manuscript.

A static version of this repository is uploaded to <https://zenodo/communities/indecol/>. It also contains the required data and the results.

## Requirement
* [hybridvut](https://gitlab.com/exiobase/hybridvut) 
* [xlsxwriter](https://pypi.org/project/XlsxWriter)
* [matplotlib](https://matplotlib.org)

## If you want to generate all zenodo output from scratch, how to use the scripts?

1. Copy the repository hybrid_mess_exio_5reg to your local system
```bash
git clone https://gitlab.com/exiobase/hybrid_mess_exio_5reg
```
2. Navigate to folder /hybrid_mess_exio_5reg; and  install the required packages in your python environment
```bash
pip install -r requirements.txt
```
3. Copy the /input data from zenodo [DOI 10.5281/zenodo.6405387](https://doi.org/10.5281/zenodo.6405386) and paste it into your local repository folder /hybrid_mess_exio_5reg. Navigate to your local repository folder /scripts and run the scripts (from your python environment):
```bash
# Exiobase and Messageix raw data is required (~1 GB).
# Running all scriptes one after another requires ~1:45 hours
# and produces 1.1 GB of output (intermediate data and results)
python create_messageVUT.py; python convert_messageVUT_to_monet.py; python create_messageF.py; python add_units_to_VUT_F.py; python aggregate_convertUnit.py; python build_HybridTables.py; python determine_footprints.py; python determine_F_mult.py; python analyze_contribution_F_mult.py; python figures.py
```

