from pathlib import Path
import os

input_folder = Path(__file__).parent / "../input_data"
output_folder = Path(__file__).parent / "../output_data"
result_folder = Path(__file__).parent / "../output_results"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

if not os.path.exists(result_folder):
    os.makedirs(result_folder)

import hybridvut as hyb
import hybridvut.preprocessing as pp
import numpy as np
import pandas as pd

# 0) Read data
# Messageix (already monetized)
V_for = pd.read_excel(
    output_folder / "V_for_agg_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1, 2],
)
U_for = pd.read_excel(
    output_folder / "U_for_agg_2020.xlsx",
    index_col=[0, 1, 2],
    header=[0, 1],
)
F_for = pd.read_excel(
    output_folder / "F_Mess_conv_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1],
)

# Exiobase
V_back = pd.read_excel(
    output_folder / "V_back_agg_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1, 2],
)
U_back = pd.read_excel(
    output_folder / "U_back_agg_2020.xlsx",
    index_col=[0, 1, 2],
    header=[0, 1],
)
F_back = pd.read_excel(
    output_folder / "F_Exio_conv_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)
F_Y_back = pd.read_excel(
    output_folder / "F_Y_Exio_conv_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)


# 1a) Create VUT instances
VUT_for = hyb.VUT(
    V=V_for,
    U=U_for,
    F=F_for,
)

VUT_back = hyb.VUT(
    V=V_back,
    U=U_back,
    F=F_back,
    F_Y=F_Y_back,
)

# 1) Create HybridTables instance
HT = hyb.HybridTables(
    foreground=VUT_for,
    background=VUT_back,
)


# 2) Define Concordance Matrices
#    Simplified approach, since the concaordance  factors are assumed
#    to be equal in all regions.
# regions:
H_reg = pd.DataFrame(
    np.array(
        [
            [1, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, 0],
        ]
    ).astype("float"),
    index=[
        "europe",
        "north_america",
        "latin_america",
        "africa_mid_east",
        "asia_ozeania",
    ],
    columns=["EUR", "NAM", "LAM", "AFR_ME", "ASI_OZ", "Trade"],
)
H_reg.index.names = ["region"]
H_reg.columns.names = ["region"]

# industries:
H_ind = pd.read_excel(
    input_folder / "H_ind_2020.xlsx",
    sheet_name="H_ind_harmo",
    index_col=[0],
    header=[0],
)
H_ind.drop(["industry"], inplace=True)
H_ind.index.names = ["industry"]
H_ind.columns.names = ["industry"]
H_ind.fillna(0.0, inplace=True)

# commodities:
H_com = pd.read_excel(
    input_folder / "H_com_unit_2020.xlsx",
    sheet_name="H_com_harmo",
    index_col=[0, 1],
    header=[0, 1],
)
H_com.columns.names = ["commodity", "unit"]
H_com.index.names = ["commodity", "unit"]
H_com.fillna(0.0, inplace=True)

# interventions:
H_int = pd.read_excel(
    input_folder / "H_int_unit_2020.xlsx",
    sheet_name="H_int_zeros",  # with or without _zeros
    index_col=[0, 1],
    header=[0, 1],
)
H_int.columns.names = ["intervention", "unit"]
H_int.index.names = ["intervention", "unit"]
H_int.fillna(0.0, inplace=True)


# H:
H = pd.DataFrame(
    np.kron(H_reg, H_ind),
    index=pd.MultiIndex.from_product([H_reg.index, H_ind.index]),
    columns=pd.MultiIndex.from_product([H_reg.columns, H_ind.columns]),
)

# H1:
import itertools

tuples_ix = []
for t in itertools.product(H_reg.columns, H_com.index.get_level_values(0), ["EUR"]):
    tuples_ix.append(t)

tuples_co = []
for t in itertools.product(H_reg.index, H_com.columns.get_level_values(0), ["EUR"]):
    tuples_co.append(t)


H1 = pd.DataFrame(
    np.kron(H_reg.transpose(), H_com),
    index=pd.MultiIndex.from_tuples(tuples_ix, names=["region", "commodity", "unit"]),
    columns=pd.MultiIndex.from_tuples(tuples_co, names=["region", "commodity", "unit"]),
)

# Mapping of traded commodities to delete
reg_map = {
    "EUR": [
        "NAM",
        "LAM",
        "AFR_ME",
        "ASI_OZ",
        "north_america",
        "latin_america",
        "africa_mid_east",
        "asia_ozeania",
    ],
    "NAM": [
        "EUR",
        "LAM",
        "AFR_ME",
        "ASI_OZ",
        "europe",
        "latin_america",
        "africa_mid_east",
        "asia_ozeania",
    ],
    "LAM": [
        "NAM",
        "EUR",
        "AFR_ME",
        "ASI_OZ",
        "north_america",
        "europe",
        "africa_mid_east",
        "asia_ozeania",
    ],
    "AFR_ME": [
        "NAM",
        "LAM",
        "EUR",
        "ASI_OZ",
        "north_america",
        "latin_america",
        "europe",
        "asia_ozeania",
    ],
    "ASI_OZ": [
        "NAM",
        "LAM",
        "AFR_ME",
        "EUR",
        "north_america",
        "latin_america",
        "africa_mid_east",
        "europe",
    ],
}


# 3) Hybridize system

V_total, U_total, F_total, Q_total, interm_tables = HT.hybridize(
    H=H,
    H1=H1,
    HF=H_int,
    delete_trade_in_reg=reg_map,
    message_exiobase=True,
    check_negative_U=False,
    return_interm_tables=True,
)


# 4) Write reults

V_total.to_excel(result_folder / "V_hybridized_2020.xlsx")
U_total.to_excel(result_folder / "U_hybridized_2020.xlsx")
F_total.to_excel(result_folder / "F_hybridized_2020.xlsx")

# write concordance matrices
try:
    interm_tables.H_ind.to_excel(result_folder / "H_ind.xlsx")
except AttributeError:
    pass

try:
    interm_tables.H_com.to_excel(result_folder / "H_com.xlsx")
except AttributeError:
    pass

try:
    interm_tables.H_int.to_excel(result_folder / "H_int.xlsx")
except AttributeError:
    pass

# write weighting factor matrices
try:
    interm_tables.T_u.to_excel(result_folder / "T_u.xlsx")
except AttributeError:
    pass

try:
    interm_tables.T_d.to_excel(result_folder / "T_d.xlsx")
except AttributeError:
    pass

try:
    interm_tables.T_int.to_excel(result_folder / "T_int.xlsx")
except AttributeError:
    pass
