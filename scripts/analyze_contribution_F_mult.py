from pathlib import Path
import os

result_folder = Path(__file__).parent / "../output_results"

if not os.path.exists(result_folder):
    os.makedirs(result_folder)

import hybridvut as hyb
import pandas as pd


V = pd.read_excel(
    result_folder / "V_hybridized_2020.xlsx",
    header=[0, 1, 2],
    index_col=[0, 1],
)
U = pd.read_excel(
    result_folder / "U_hybridized_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1, 2],
)
F = pd.read_excel(
    result_folder / "F_hybridized_2020.xlsx",
    header=[0, 1],
    index_col=[0, 1],
)


VUT_total = hyb.VUT(V=V, U=U, F=F)

# Intervention-multipliers:
# commodity
F_mult = VUT_total.calc_F_mult(
    tech_assumption="industry",
    raw_factors="per-ind",
    multipliers="per-com",
    impacts=False,
    contribution={
        "category": ["CO2"],
        "com_or_ind": [
            "final|biomass",
            "final|coal",
            "final|d_heat",
            "final|electr",
            "final|ethanol",
            "",
            "final|fueloil",
            "final|gas",
            "final|hydrogen",
            "final|lh2",
            "final|lightoil",
            "final|methanol",
        ],
    },
)

F_mult.to_excel(result_folder / "Contr_F_mult_com_2020.xlsx")

# industry
categories = [
    "CO2 - combustion - air",
    "CH4 - combustion - air",
    "N2O - combustion - air",
    "Taxes less subsidies on products purchased: Total",
    "Other net taxes on production",
    "Compensation of employees; wages, salaries, & employers' social contributions: Low-skilled",
    "Compensation of employees; wages, salaries, & employers' social contributions: Medium-skilled",
    "Compensation of employees; wages, salaries, & employers' social contributions: High-skilled",
    "Operating surplus: Consumption of fixed capital",
    "Employment hours: Low-skilled male",
    "Employment hours: Low-skilled female",
    "Employment hours: Medium-skilled male",
    "Employment hours: Medium-skilled female",
    "Employment hours: High-skilled male",
    "Employment hours: High-skilled female",
    "Domestic Extraction Used - Metal Ores - Bauxite and aluminium ores",
    "Domestic Extraction Used - Metal Ores - Copper ores",
    "Domestic Extraction Used - Metal Ores - Gold ores",
    "Domestic Extraction Used - Metal Ores - Iron ores",
    "Domestic Extraction Used - Metal Ores - Lead ores",
    "Domestic Extraction Used - Metal Ores - Nickel ores",
    "Domestic Extraction Used - Metal Ores - Other non-ferrous metal ores",
    "Domestic Extraction Used - Metal Ores - PGM ores",
    "Domestic Extraction Used - Metal Ores - Silver ores",
    "Domestic Extraction Used - Metal Ores - Tin ores",
    "Domestic Extraction Used - Metal Ores - Uranium and thorium ores",
    "Domestic Extraction Used - Metal Ores - Zinc ores",
]

industries = [
    "bio_hpl|M1",
    "bio_ppl|M1",
    "coal_extr_ch4|M1",
    "coal_extr|M1",
    "coal_hpl|M1",
    "coal_ppl_u|M1",
    "coal_ppl|M1",
    "coal_t_d|M1",
    "elec_t_d|M1",
    "eth_bio|M1",
    "foil_hpl|M1",
    "foil_ppl|M1",
    "gas_cc|M1",
    "gas_ct|M1",
    "gas_extr_1|M1",
    "gas_extr_2|M1",
    "gas_extr_3|M1",
    "gas_hpl|M1",
    "geo_hpl|M1",
    "geo_hpl|M1",
    "geo_ppl|M1",
    "hydro_hc|M1",
    "hydro_lc|M1",
    "lignite_extr|M1",
    "loil_ppl|M1",
    "meth_ng|M1",
    "nuc_hc|M1",
    "nuc_lc|M1",
    "oil_extr_1_ch4|M1",
    "oil_extr_1|M1",
    "oil_extr_2_ch4|M1",
    "oil_extr_2|M1",
    "oil_extr_3|M1",
    "oil_extr_4|M1",
    "ref_hil|M1",
    "ref_lol|M1",
    "solar_pv_ppl|M1",
    "wind_ppl|M1",
    "wind_ref1|M1",
    "wind_res1|M1",
    "wind_res3|M1",
]

F_mult = pd.DataFrame({})
for cat in categories:
    df = VUT_total.calc_F_mult(
        tech_assumption="industry",
        raw_factors="per-ind",
        multipliers="per-ind",
        impacts=False,
        contribution={"category": [cat], "com_or_ind": industries},
    )
    F_mult = F_mult.append(df)

F_mult.to_excel(result_folder / "Contr_F_mult_ind_2020.xlsx")
