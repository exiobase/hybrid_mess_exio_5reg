from pathlib import Path
import os

input_folder = Path(__file__).parent / "../input_data/EXIOBASE"
output_folder = Path(__file__).parent / "../output_data"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

import pandas as pd

# Messageix (already monetized)
V_for = pd.read_excel(
    output_folder / "V_Mess_mon_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1],
)
U_for = pd.read_excel(
    output_folder / "U_Mess_mon_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1],
)
F_for = pd.read_excel(
    output_folder / "F_Mess_2020.xlsx",
    index_col=[0],
    header=[0, 1],
)

# Exiobase
V_back = pd.read_csv(
    input_folder / "supply_2020.csv",
    sep="\t",
    index_col=[0, 1],
    header=[0, 1],
    low_memory=False,
)
V_back.index.names = ["region", "commodity"]
V_back.columns.names = ["region", "industry"]
V_back = V_back.transpose()
U_back = pd.read_csv(
    input_folder / "use_2020.csv",
    sep="\t",
    index_col=[0, 1],
    header=[0, 1],
    low_memory=False,
)
U_back.index.names = ["region", "commodity"]
U_back.columns.names = ["region", "industry"]
F_back = pd.read_table(
    input_folder / "F_2020.txt",
    header=[0, 1],
    index_col=0,
)
F_back.index.names = ["intervention"]
F_back.columns.names = ["region", "industry"]

F_Y_back = pd.read_table(
    input_folder / "F_Y_2020.txt",
    header=[0, 1],
    index_col=0,
)
F_Y_back.index.names = ["intervention"]
F_Y_back.columns.names = ["region", "demand category"]


# 1) Add units to dataframes
units_c_for = ["USD"] * len(U_for.index)
units_c_back = ["EUR"] * len(U_back.index)
units_i_for = ["kt", "Mt C", "kt"]  # for: ["CH4", "CO2", "N20"]
units_i_back = pd.read_csv(
    input_folder / "unit_2020.txt",
    sep="\t",
    header=None,
)
units_i_back.drop(index=0, inplace=True)
units_i_back.columns = ["intervention", "unit"]
units_i_back.set_index("intervention", inplace=True)

U_for.set_index([U_for.index, pd.Series(units_c_for, name="unit")], inplace=True)

V_for = V_for.transpose()
V_for.set_index([V_for.index, pd.Series(units_c_for, name="unit")], inplace=True)
V_for = V_for.transpose()

F_for.set_index([F_for.index, pd.Series(units_i_for, name="unit")], inplace=True)


U_back.set_index([U_back.index, pd.Series(units_c_back, name="unit")], inplace=True)

V_back = V_back.transpose()
V_back.set_index([V_back.index, pd.Series(units_c_back, name="unit")], inplace=True)
V_back = V_back.transpose()

units_i_back_2 = F_back.join(units_i_back)
F_back.set_index([F_back.index, units_i_back_2["unit"]], inplace=True)

units_i_back_2 = F_Y_back.join(units_i_back)
F_Y_back.set_index([F_Y_back.index, units_i_back_2["unit"]], inplace=True)


# Write data
V_for.to_excel(output_folder / "V_for_unit_2020.xlsx")
U_for.to_excel(output_folder / "U_for_unit_2020.xlsx")
F_for.to_excel(output_folder / "F_for_unit_2020.xlsx")


V_back.to_excel(output_folder / "V_back_unit_2020.xlsx")

writer = pd.ExcelWriter(
    output_folder / "U_back_unit_2020.xlsx",
    engine="xlsxwriter",
)
U_back.to_excel(writer)
writer.book.use_zip64()
writer.save()

F_back.to_excel(output_folder / "F_back_unit_2020.xlsx")
F_Y_back.to_excel(output_folder / "F_Y_back_unit_2020.xlsx")
