This folder includes scripts to manage the exemplary data.
The scripts are run in the following order:

1) create_messageVUT.py
2) convert_messageVUT_to_monet.py
3) create_messageF.py
4) add_units_to_VUT_F.py
5) aggregate_convertUnit.py
------ End of preprocessing ------
6) build_HybridTables.py
7) determine_footprints.py
7b) (optional) determine_F_mult.py
7c) (optional) analyze_contribution_F_mult.py
8) figures.py

