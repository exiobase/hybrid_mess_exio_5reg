"""Convert Message Make and Use Tables to monetary

This script converts the forground sytem of MessageEU20 into monetary units.
Therefore, the results of MessageEU20 are used (PRICE_COMODITY). 
"""

from pathlib import Path
import os

input_folder = Path(__file__).parent / "../input_data/MESSAGE"
output_folder = Path(__file__).parent / "../output_data"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

import hybridvut as hyb
import hybridvut.preprocessing as pp
import numpy as np
import pandas as pd

# Read physical tables
V_for = pd.read_excel(
    output_folder / "V_Mess_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1],
)
U_for = pd.read_excel(
    output_folder / "U_Mess_2020.xlsx",
    index_col=[0, 1],
    header=[0, 1],
)

# Read price data from PRICE_COMMODITY (USD/ GWa)
df = pd.read_excel(
    input_folder / "PRICE_COMMODITY_ENGAGE_SSP2_v4.1.2_R12_baseline_2020.xlsx"
)

# Create price vector by using the appropriate index
ix = U_for.index
p = pp.p_messageix_from_PRICE_COMMODITY(df, 2020, multiindex=ix)

# Initialize VUT system instance
VUT_for = hyb.VUT(V=V_for, U=U_for)

# Monetize tables (only forground system)
V_mon, U_mon = VUT_for.monetize(price_vector=p)

# Convert from M.EUR to EUR (to have the same unit as in Exiobase)
V_mon = V_mon * 1000000
U_mon = U_mon * 1000000

# Write monetized tables
V_mon.to_excel(output_folder / "V_Mess_mon_2020.xlsx")
U_mon.to_excel(output_folder / "U_Mess_mon_2020.xlsx")
p.to_excel(output_folder / "p_Mess_2020.xlsx")
